def equal(list1,list2):
	if len(list1)==len(list2):
		print("Lists are of same length")
	else:
		print("Lists are of different length")
def sum_list(list1,list2):
	if sum(list1)==sum(list2):
		print("Lists sum to the same value")
	else:
		print("Lists do not sum to the same value")
def occurence(list1,list2):
	for i in list1:
		if i in list2:
			print("Value ",i," occur in both lists")
print("Enter the first list : ")
list1=[int(j) for j in input().split()]
print("Enter the second list : ")
list2=[int(j) for j in input().split()]
equal(list1,list2)
sum_list(list1,list2)
occurence(list1,list2)

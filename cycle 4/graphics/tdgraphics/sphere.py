from math import pi
def area_sph(r):
	return 4*pi*r*r
def peri_sph(r):
	return 2*pi*r
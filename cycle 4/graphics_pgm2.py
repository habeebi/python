from graphics import circle,rectangle as rect
from graphics.tdgraphics import cuboid as c
from graphics.tdgraphics.sphere import *

print("\n-----circle with r=10-----")
print('Area =',circle.area_cir(10))
print('Perimeter =',circle.peri_cir(10))
 
print('\n-----rectangle with l=10,b=5-----')
print('Area =',rect.area_rec(10,5))
print('Perimeter =',rect.peri_rec(10,5))


print('\n-----cuboid with l=15,b=5,h=10-----')
print('Area =',c.area_cub(15,5,10))
print('Perimeter of cuboid=',c.peri_cub(15,5,10))

print('\n-----sphere with r=10-----')
print('Area=',area_sph(10))
print('Perimeter=',peri_sph(10))